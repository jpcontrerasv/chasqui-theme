<div class="clearfix"></div>
<div id="pre-footer" class="container-fluid bg-naranjo">
        <div class="col-lg-6 logo-footer">
            <img src="<?php bloginfo('template_url') ?>/img/logo-footer.svg" alt="Chasqui">
        </div>
        <div class="col-lg-3 somos">
            <h5>somos</h5>
            
            <p>Chasqui Ediciones nace a raíz de la necesidad de dotar de imagen y forma a los distintos proyectos musicales que componen la escena musical independiente.</p>


            <p>Ofrecemos además una vitrina virtual para poner en venta productos de autores locales que dificilmente encontrarás en otra parte.</p>
            </div>
        <div class="col-lg-3 sub-menu">
            <h5>tienda</h5>
            <ul>
                <?php wp_nav_menu( array( 
                'menu' => 'footer',
                //'menu_class' => 'nav fleft hidden-lg hidden-md',
                'container'       => '',
                'items_wrap' => '%3$s', 
                'container_class' => false,
                ) ); ?>
                <!--<li><a href="#">Mi cuenta</a></li>
                <li><a href="#">Preguntas Frecuentes</a></li>
                <li><a href="#">Envíos</a></li>

                <li><a href="#">Condiciones</a></li>-->
            </ul>
            </div>
        </div>

    <footer class="container-fluid bg-minero">         
        
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <p class="slogan">Chasqui Ediciones 2018 · imagen, forma & ruido</p>
        </div>
        
        <div class="col-lg-6 col-md-6 col-md-offset-2 col-sm-6 col-xs-12">
            <p><a href="mailto:contacto@chasquiediciones.cl"> contacto@chasquiediciones.cl</a>  ·  <a href="https://goo.gl/maps/LMw61T5cU652" target="_blank" title="Abrir en Google Maps"> Serrano 591, oficina 12 C  ·  Valparaíso, Chile</a></p>
        </div>
        
    </footer>
</div>


    <?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111932169-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111932169-1');
</script>


  </body>
</html>