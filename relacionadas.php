
<?php $orig_post = $post;
global $post;
$tags = wp_get_post_tags($post->ID);
if ($tags) {
$tag_ids = array();
foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
$args=array(
'tag__in' => $tag_ids,
'post__not_in' => array($post->ID),
'posts_per_page'=>3, // Number of related posts that will be shown.
'caller_get_posts'=>1
);
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {


while( $my_query->have_posts() ) {
$my_query->the_post(); ?>

        <div class="item-relacionada box fleft fwidth">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('thumb-blog'); ?>
            </a>
            <div class="fwidth fleft txt">
                <span class="fecha">
                    <?php the_time('M'); ?> <?php the_time('j'); ?>, <?php the_time('Y'); ?>
                </span>
                <h5>
                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h5>
            </div>
        </div>
<? }
}
}
$post = $orig_post;
wp_reset_query(); ?>