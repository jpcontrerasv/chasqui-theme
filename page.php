<?php include 'header.php' ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            


<div id="wrapper" class="box fleft fwidth ">
    <div class="container">
        
        <?php if (is_page('contacto')) { ?>
        <div id="pagina-contacto" class="col-lg-8 col-lg-offset-2 single-galeria">
            
            <? } else { ?>
        <div  class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 single-galeria">
            
            <? } ?>
            
            <div class="col-lg-12 txt">
                <h1><?php the_title(); ?></h1>
                <br>
                <div class="clearfix"></div>
                <?php the_content(); ?>
            </div>
            
        </div>
        
        
    </div>
</div>

<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
<?php include 'footer.php' ?>