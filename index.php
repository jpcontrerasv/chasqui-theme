<?php include 'header.php' ?>
<!--test-->

<div class="box fwidth bg-naranjo">
    
    <div id="home-blocks" class="container">

        <div class="col-lg-6 txt">
            
            <p class="txt-white">Chasqui Ediciones es un estudio de diseño y oficios gráficos especializado en la industria musical.</p><br><br>
            
            <div class="img-home fright animated fadeIn delay-1s">
                
                <a href="/servicios">servicios</a>
                
                <img src="<?php the_field('imagen_servicios', 'option'); ?>" alt="">
            </div>
            
        </div>
        
        <div class="col-lg-6 item-home">
            
            <div class="img-home fleft animated fadeIn delay-2s">
                
                <a href="/proyectos">proyectos</a>
                <img src="<?php the_field('imagen_proyectos', 'option'); ?>" alt="">
                
            </div>
            
            <div class="img-home fleft animated fadeIn delay-3s">
                
                <a href="/tienda">tienda</a>
            
                <img src="<?php the_field('imagen_tienda', 'option'); ?>" alt="">
            
            </div>
            
        </div>


    </div>
    
</div>

        <div class="clearfix"></div>

    
<section id="tienda-home" class="container-fluid no-column">
    
    <?php /*
    <div class="link-visitar-proyectos fwidth">
                <div class="container-fluid">
                    <div class="col-lg-12">
                    <div class="caja-link box fright text-right">
                    <p>
                        <a href="/tienda">Visitar</a>
                        <span class="clearfix"></span>
                        <a href="/tienda">Tienda</a>
                    </p>
                    <div class="svg">
                    <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="01.--home" transform="translate(-1210.000000, -112.000000)" fill="#363636">
                            <g id="Group-4-Copy-6" transform="translate(1210.000000, 112.000000)">
                                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                                </g>
                            </g>
                        </g>
                    </g>
                    </svg>
                    </div>
                    
                    </div>
                    </div>
                </div>
            </div>
        */?>
    
    <?php if( have_rows('apariencia_principal', 'option') ): while( have_rows('apariencia_principal', 'option') ): the_row(); ?>		
        
        
    <div id="img" class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-column" style="background-image:url(<?php the_sub_field('imagen_principal_tienda_home', 'option'); ?>);">&nbsp;</div>
        
    <div id="contenido" class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="background-color:<?php the_sub_field('color_de_fondo'); ?>;">
            
            	
        <?php endwhile; endif; ?>
            
            
        <div class="row">
                
            <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 text-center">
                
                <?php if ( get_field( 'logo_imagen_tienda_home', 'option' ) ): ?>
 
                <img src="<?php the_field('logo_imagen_tienda_home', 'option'); ?>" alt="">
                    
                <?php else: // field_name returned false ?>
                    
                <?php if( have_rows('titular_tienda_home', 'option') ): while( have_rows('titular_tienda_home', 'option') ): the_row(); ?>		
                    
                <h1 style="color:<?php the_sub_field('color_del_titular'); ?>;"><?php the_sub_field('titulo_tienda_home'); ?></h1>

                    
                <?php endwhile; ?>

                    
                <?php endif; ?>                    
                    
 

                    
                <?php endif; // end of if field_name logic ?>
                    
                    <?php if( have_rows('bajada_del_producto', 'option') ): while( have_rows('bajada_del_producto', 'option') ): the_row(); ?>		
                    <p style="color:<?php the_sub_field('color_del_texto'); ?>;" ><?php the_sub_field('texto_tienda_home'); ?></p>
                    <?php endwhile; ?>

                    <?php endif; ?>                    

                    <?php if( have_rows('boton_principal', 'option') ): while( have_rows('boton_principal', 'option') ): the_row(); ?>
                    <a style="border-color:<?php the_sub_field('color_del_boton'); ?>; color:<?php the_sub_field('color_del_boton'); ?>;" href="<?php the_sub_field('link_al_producto'); ?>"><?php the_sub_field('texto_del_boton'); ?></a>
                    
                    <?php endwhile; ?>

                    <?php endif; ?>     
                    
                </div>
                
            </div>
            
        </div>
        
        </section>
        <div class="clearfix"></div>



    <section id="blog-home" class="box fleft fwidth bg-naranjo">
        
        <div class="container-fluid">
            
            
            <div class="box fleft fwidth text-right">
                <div class="col-lg-4 col-lg-offset-1 col-xs-10 col-xs-offset-1 no-column">
                </div>
                <div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1no-column pr0">
                    <h1 class="txt-white" style="color:#FFF; margin:0; margin-right:-15px;">bitácora</h1>
                </div>
            </div>
            
        </div>
            
            <?php 
            $temp = $wp_query; 
            $wp_query = null; 
            $wp_query = new WP_Query(); 
            $wp_query->query('posts_per_page=8&post_type=post'.'&paged='.$paged); 

            while ($wp_query->have_posts()) : $wp_query->the_post(); 
            ?>
            
            <div class="box fleft fwidth item-portada-blog">
                <div class="col-lg-4 col-lg-offset-1 col-xs-10 col-xs-offset-1 no-column img" style="background-image:url(<?php the_post_thumbnail_url(); ?>);" >
                    <a href="<?php the_permalink(); ?>">&nbsp;</a>
                </div>
                <div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1 texto">
                    <span class="fecha"><?php the_time('M'); ?> <?php the_time('j'); ?>, <?php the_time('Y'); ?></span>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                    <p><a href="<?php the_permalink(); ?>">ver artículo</a></p>
                </div>
            </div>
            <?php endwhile; ?>
<?php 
$wp_query = null; 
$wp_query = $temp;  // Reset
?>
            
            
        </div>
    </section>
      
<?php include 'footer.php' ?>