<?php
function twentythirteen_setup() {

	add_theme_support( 'automatic-feed-links' );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Menu principal', 'twentyfifteen' ),
        'secondary' => __( 'Menu Footer', 'twentyfifteen' ),
	) );
    
    	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );


	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 800, 9999, true );
    add_image_size('thumb-blog', 900, 9999 );

    
	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'twentythirteen_setup' );


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page('Redes Sociales');
    acf_add_options_page('Contenido Home');
}

add_filter('get_post_metadata', function($value, $object_id, $meta_key, $single) {
	if ($meta_key !== '_thumbnail_id' || $value) {
		return $value;
	}

	preg_match('~<img[^>]+wp-image-(\\d+)~', get_post_field('post_content', $object_id), $matches);
	if ($matches) {
		return $matches[1];
	}
	return $value;
}, 10, 4);
//soporte woocommerce 
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

function smallenvelop_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Header Sidebar', 'smallenvelop' ),
        'id' => 'header-sidebar',
        'before_widget' => '<div class="widget-cart">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => '',
    ) );
}
add_action( 'widgets_init', 'smallenvelop_widgets_init' );
function x_my_custom_widgets_init() {

  register_sidebar( array(
    'name'          => __( 'Sección lateral de la tienda', '__x__' ),
    'id'            => 'sidebar-my-custom-shop',
    'description'   => __( 'Aparece en el lateral de la tienda.', '__x__' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="h-widget">',
    'after_title'   => '</h4>',
  ) );

}

add_action( 'widgets_init', 'x_my_custom_widgets_init' );

//remover toolbar 
add_filter('show_admin_bar', '__return_false');

//limitar taxonomia
add_action( 'pre_insert_term', function ( $term, $taxonomy )
{
    return ( 'destacada_blog' === $taxonomy )
        ? new WP_Error( 'term_addition_blocked', __( 'No puedes agregar más opciones' ) )
        : $term;
}, 0, 2 );

// you will probably need to run this once to reset the cache
// wp_cache_flush();


//Scripts & styles 
function chasqui_scripts_styles() {
    //styles
    
    
    wp_register_style('cha-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('cha-bootstrap');
    
    wp_register_style('cha-fonta', get_template_directory_uri() . '/css/font-awesome.css');
    wp_enqueue_style('cha-fonta');
    
    wp_register_style('cha-animate', get_template_directory_uri() . '/css/animate.css');
    wp_enqueue_style('cha-animate');
    
    wp_register_style('cha-slicktheme', get_template_directory_uri() . '/css/slick-theme.css');
    wp_enqueue_style('cha-slicktheme');
    
    wp_register_style('cha-slick', get_template_directory_uri() . '/css/slick.css');
    wp_enqueue_style('cha-slick');
    
    wp_register_style('cha-feath', get_template_directory_uri() . '/css/featherlight.css');
    wp_enqueue_style('cha-feath');
    
    wp_register_style('cha-feathgallery', get_template_directory_uri() . '/css/featherlight.gallery.min.css');
    wp_enqueue_style('cha-feathgallery');
    

          
//scripts
    
    wp_register_script( 'cha-jqry', get_template_directory_uri() . '/js/jquery.min.js','','null',true );
    wp_enqueue_script( 'cha-jqry');
    
    wp_register_script( 'cha-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js','','null',true );
    wp_enqueue_script( 'cha-bootstrap');
    
    wp_register_script( 'cha-featherlight', get_template_directory_uri() . '/js/featherlight.js','','null',true );
    wp_enqueue_script( 'cha-featherlight');
    
    
    wp_register_script( 'cha-featherlightgaljs', get_template_directory_uri() . '/js/featherlight.gallery.min.js','','null',true );
    wp_enqueue_script( 'cha-featherlightgaljs');
    
    wp_register_script( 'cha-skro', get_template_directory_uri() . '/js/skrollr.min.js','','null',true );
    wp_enqueue_script( 'cha-skro');
    
    
    wp_register_script( 'cha-slickjs', get_template_directory_uri() . '/js/slick.min.js','','null',true );
    wp_enqueue_script( 'cha-slickjs');   

    
    wp_register_script( 'cha-isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js','','null',true );
    wp_enqueue_script( 'cha-isotope');   
    
    wp_register_script( 'cha-typer', get_template_directory_uri() . '/js/typer.js','','null',true );
    wp_enqueue_script( 'cha-typer');   
    
    
     wp_register_script( 'cha-masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js','','null',true );
    wp_enqueue_script( 'cha-masonry');
    
    wp_register_script( 'cha-macy', get_template_directory_uri() . '/js/macy.js','','null',true );
    wp_enqueue_script( 'cha-macy');
    
    
    wp_register_script( 'cha-imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js','','null',true );
    wp_enqueue_script( 'cha-imagesloaded');
    
        
    wp_register_script( 'cha-allscripts', get_template_directory_uri() . '/js/scripts.js','','null',true );
    wp_enqueue_script( 'cha-allscripts');
    

    
}
add_action( 'wp_enqueue_scripts', 'chasqui_scripts_styles' );

//Page Slug Body Class
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 

//paginacion
function wpbeginner_numeric_posts_nav() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="navigation"><ul class="page-numbers">' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li class="arrowed"><span>%s</span></li>' . "\n", get_previous_posts_link('←') );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li class="arrowed">%s</li>' . "\n", get_next_posts_link('→') );
 
    echo '</ul></div>' . "\n";
 
}

//remover p vacíos
function remove_empty_p( $content ) {
	$content = force_balance_tags( $content );
	$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
	$content = preg_replace( '~\s?<p>(\s| )+</p>\s?~', '', $content );
	return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);

//vaciar carrito después de comprar
add_action( 'woocommerce_checkout_order_processed', 'order_received_empty_cart_action', 10, 1 );
// or 
// add_action( 'woocommerce_thankyou', 'order_received_empty_cart_action', 10, 1 );

//remover envío desde el carrito
function disable_shipping_calc_on_cart( $show_shipping ) {
    if( is_cart() ) {
        return false;
    }
    return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );
 