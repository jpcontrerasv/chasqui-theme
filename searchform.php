<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
    <input type="search" class="search-field form-control" placeholder="Escribe tu búsqueda" value="" name="s" title="Buscando:">
    <input type="submit" class="search-submit" value="Buscar" />
</form>