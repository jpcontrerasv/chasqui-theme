<?php include 'header.php' ?>


<div id="wrapper" class="box fleft fwidth">
    
    <div class="container-fluid">
        <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 woocommerce">
            
            <!-- si es la tienda-->

            <?php if (is_shop()) { ?>
            
            <?php /*
            <div id="pagina-tienda" class="box fleft fwidth">
                <div id="slider-productos" class="col-lg-12 no-column slider-container">

            <?php 
              $temp = $wp_query; 
              $wp_query = null; 
              $wp_query = new WP_Query(); 
              $wp_query->query('posts_per_page=10&post_type=product'); while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                    <div class="slide" style="background-image:url(<?php the_field('imagen_principal_para_slider_woocommerce'); ?>);">
                        <!--index-->
                        <!--<div class="wrapper-title fleft fwidth">
                            <div class="container">
                                <div class="col-lg-12">
                                    <a class="titulo" href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </div>
                            </div>
                        </div>-->
                    </div>

                    <?php endwhile; ?>
            <?php 
            $wp_query = null; 
            $wp_query = $temp;  // Reset
            ?>                  



                </div>
                <div class="titulo-tienda box fleft fwidth">
                    <h1 id="consulte">consulte sin compromiso</h1>
                </div>
            </div>
            */?>
            
            <div class="row">
                <div id="sidebar" class="col-lg-3">
                    <?php dynamic_sidebar( 'sidebar-my-custom-shop' ); ?>
                </div>
                <div class="col-lg-9">
                    <?php woocommerce_content(); ?>
                </div>
            </div>

            <? } else { ?>

            
            <?php woocommerce_content(); ?>
            
            <? } ?>
            
            
            
        </div>
        
        
    </div>
</div>

<?php include 'footer.php' ?>