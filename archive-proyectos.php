<?/*Template name: Proyectos*/?>
<?php include 'header.php' ?>

<div id="wrapper" class="box fleft fwidth">
    <div class="container-fluid">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 filtros-galeria">


            
    <div class="button-group filters-button-group">
        <button class="button is-checked" data-filter="*">todo</button>
<?php $wcatTerms = get_terms('tipodeobjeto', array('hide_empty' => 1, 'parent' =>0)); 
foreach($wcatTerms as $wcatTerm) : 
?>
        <button class="button" data-filter=".<?php echo $wcatTerm->name; ?>"><?php echo $wcatTerm->name; ?></button>

<?php endforeach; ?>  
        
        
    </div>


<div class="grid fleft fwidth">
    
    <?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            
 
    
<div class="element-item <?php $terms = get_the_terms( $post->ID , 'tipodeobjeto' ); 
                    foreach ( $terms as $term ) {
                        $term_link = get_term_link( $term, 'tipodeobjeto' );
                        if( is_wp_error( $term_link ) )
                        continue;
                    echo ' ' . $term->name . '';
                    } 
                ?> no-column">
  <div class="wrapper-item" style="background-image:url(<?php the_post_thumbnail_url(); ?>);">
      <div class="txt">
          <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
              
              
              
          </p>
      </div>
  </div>

</div>
    
<?php endwhile; ?>
            </div>
            <div class="clearfix"></div>
    <?php wpbeginner_numeric_posts_nav(); ?>
<?php else : ?>
<?php endif; ?> 

            
        </div>
        
        
    </div>
</div>

<?php include 'footer.php' ?>