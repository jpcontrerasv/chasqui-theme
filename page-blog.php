<?php include 'header.php' ?>
    <section id="header-blog" class="box fleft fwidth destacada bg-white ">
        <div class="container-fluid">
            <div class="col-lg-12">
                <h6>Destacada</h6>
            </div>
            <div class="clearfix"></div>
            <?php 
            query_posts( array(
            'post_type' => 'post',
            'taxonomy'	=> 'destacada_blog',
            'term'	=> 'destacada',
            'posts_per_page'	=> 1,
            ));
            if (have_posts()) :
            while (have_posts()) : the_post();  
            ?>
            
            <div class="box fleft fwidth item-portada-blog">
                <div class="col-lg-6 pr0 img" style="background-image:url(<?php the_post_thumbnail_url('full'); ?>);">
                    <a href="<?php the_permalink(); ?>">
                        &nbsp;
                    </a>
                </div>
                <div class="col-lg-6 texto">
                    <span class="fecha"><?php the_time('M'); ?> <?php the_time('j'); ?>, <?php the_time('Y'); ?></span>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                    <p><a href="<?php the_permalink(); ?>">ver artículo</a></p>
                </div>
            </div>
            <?php endwhile; endif;  ?>
            <?php wp_reset_query(); ?>
 
            
            
        </div>
    </section>


    <section id="blog-home" class="box fleft fwidth bg-plata" style="padding-top:24px;">
        <div class="container-fluid">
            <div class="col-lg-12">
                <h6>Recientes</h6>
            </div>
            <div class="clearfix"></div>
            <?/*            
            <?php 
              $temp = $wp_query; 
              $wp_query = null; 
              $wp_query = new WP_Query(); 
              $wp_query->query('posts_per_page=8&post_type=post'.'&paged='.$paged); 

              while ($wp_query->have_posts()) : $wp_query->the_post(); 
            ?>*/ ?>
            
            <?php 
                    query_posts( array(
                    'post_type' => 'post',
                    'posts_per_page'	=> 4,
                    'paged' => $paged,					
					'tax_query'	=> array(
						array(
							'taxonomy'  => 'destacada_blog',
							'terms'     => 'destacada', 
							'operator'  => 'NOT IN')
							),
                    ));
                    if (have_posts()) :
                    while (have_posts()) : the_post();  
                    ?>
            
            
            
            <div class="box fleft fwidth item-portada-blog">
                <div class="col-lg-4 col-lg-offset-1 col-xs-10 col-xs-offset-1 no-column img" style="background-image:url(<?php the_post_thumbnail_url('thumb-blog'); ?>);" >
                    <a href="<?php the_permalink(); ?>">&nbsp;</a>
                </div>
                <div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1 texto">
                    <span class="fecha"><?php the_time('M'); ?> <?php the_time('j'); ?>, <?php the_time('Y'); ?></span>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>
                    <p><a href="<?php the_permalink(); ?>">ver artículo</a></p>
                </div>
            </div>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
<?php wp_reset_query(); ?>
             
            
        </div>
    </section>


<?php include 'footer.php' ?>