<?php
/*
 * Template Name: Servicios
 *
 */
?>
<?php include 'header.php' ?>
<div id="wrapper" class="servicios box fleft fwidth">
    
    <div class="container-fluid">
        <div id="tab-izquierda" class="col-lg-6">
            <h1 id="diseno" class="on">diseño</h1>
            <h1 id="manufactura">manufactura</h1>
        </div>
        <div id="tab-derecha" class="col-lg-6">
            <div id="tab-diseno" class="box fleft fwidth">
                <div id="wrapper-maso" class="box fleft fwidth">
                    
                    <?php if( have_rows('servicio_de_diseno') ): while ( have_rows('servicio_de_diseno') ) : the_row(); ?>

        <div class="col-lg-6 item-servicio">
                    <img src="<? the_sub_field('icono_dis'); ?>" alt="">
                    <h3><? the_sub_field('nombre_del_servicio_diseno'); ?></h3>
                    <? the_sub_field('texto_del_servicio_diseno'); ?>
                </div>

    <? endwhile; else : endif; ?>
                    
                
                </div>
                <!--wrapper maso-->
                <div class="clearfix"></div>
                <div class="caja-link box fright text-right">
                <p>
                    <a href="/contacto">Quiero cotizar</a> 
                    <span class="clearfix"></span>
                    <a href="/contacto">Servicios de diseño</a>
                </p>
                <div class="svg">
                    <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="01.--home" transform="translate(-1210.000000, -112.000000)" fill="#363636">
                            <g id="Group-4-Copy-6" transform="translate(1210.000000, 112.000000)">
                                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
                </div>
            </div>
            </div>
            
            <div id="tab-manufactura" class="box fleft fwidth off">
                <div id="wrapper-maso-manu" class="box fleft fwidth" >
                    
                <?php if( have_rows('servicio_de_manufactura') ): while ( have_rows('servicio_de_manufactura') ) : the_row(); ?>

        <div class="col-lg-6 item-servicio">
                    <img src="<? the_sub_field('icono_manu'); ?>" alt="">
                    <h3><? the_sub_field('nombre_del_servicio_manu'); ?></h3>
                    <? the_sub_field('texto_del_servicio_manu'); ?>
                </div>

    <? endwhile; else : endif; ?>
                    
                
                </div>
                <div class="clearfix"></div>
                <div class="caja-link box fright text-right">
                <p>
                    <a href="/contacto">Quiero cotizar</a> 
                    <span class="clearfix"></span>
                    <a href="/contacto">Servicios de manufactura</a>
                </p>
                <div class="svg">
                    <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="01.--home" transform="translate(-1210.000000, -112.000000)" fill="#363636">
                            <g id="Group-4-Copy-6" transform="translate(1210.000000, 112.000000)">
                                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
                </div>
            </div>
            </div>
            
        </div>
    </div>
    
</div>
<?php include 'footer.php' ?>