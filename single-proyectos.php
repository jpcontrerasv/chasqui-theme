<?php include 'header.php' ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            
 
<div id="wrapper" class="galeria box fleft fwidth">
    <div class="container-fluid">
        
        <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 single-galeria">
            <div class="col-lg-5 txt">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
                
                <ul class="info-adicional">
                    
                    <?php

// check if the repeater field has rows of data
if( have_rows('informacion_adicional') ):

 	// loop through the rows of data
    while ( have_rows('informacion_adicional') ) : the_row(); ?>

        <li><span><? the_sub_field('titulo'); ?>:</span> 
            <span class="clearfix"></span>
            <? the_sub_field('informacion'); ?>
                    </li>
       

    <?php endwhile; else : endif; ?>
                    
                    
                    
                </ul>
                
                
                <div class="caja-link box fleft text-left hidden-xs">
                <div class="svg">
                    
<svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="05.-proyectos-(proyecto-ejemplo)" transform="translate(-47.000000, -879.000000)" fill="#363636">
            <g id="Group-4-Copy-6" transform="translate(57.000000, 889.000000) rotate(-180.000000) translate(-57.000000, -889.000000) translate(47.000000, 879.000000)">
                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                </g>
            </g>
        </g>
    </g>
</svg>
                </div>
                <p>
                    <a href="/proyectos">Volver</a>
                    <span class="clearfix"></span>
                    <a href="/proyectos" >a proyectos</a>
                </p>
            </div>
                
            </div>
            
            <div class="col-lg-6 col-lg-offset-1 fotos">
                                    <?php

// check if the repeater field has rows of data
if( have_rows('fotos_del_proyecto') ):

 	// loop through the rows of data
    while ( have_rows('fotos_del_proyecto') ) : the_row(); ?>
<a class="gallery2" href="<?php the_sub_field('imagenproyecto'); ?>"><img src="<?php the_sub_field('imagenproyecto'); ?>" alt=""></a>
                
        
       <!--segundo test de commit con pipelines-->

    <? endwhile;

else :

    // no rows found

endif;

?>
                
                <br><br><br>
                
                <div class="caja-link box fleft text-left hidden-lg hidden-md hidden-sm">
                <div class="svg">
                    
<svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="05.-proyectos-(proyecto-ejemplo)" transform="translate(-47.000000, -879.000000)" fill="#363636">
            <g id="Group-4-Copy-6" transform="translate(57.000000, 889.000000) rotate(-180.000000) translate(-57.000000, -889.000000) translate(47.000000, 879.000000)">
                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                </g>
            </g>
        </g>
    </g>
</svg>
                </div>
                <p>
                    <a href="/proyectos">Volver</a>
                    <span class="clearfix"></span>
                    <a href="/proyectos" >a proyectos</a>
                </p>
            </div>
                
                
              
            </div>
        </div>
        
        
    </div>
</div>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
<?php include 'footer.php' ?>