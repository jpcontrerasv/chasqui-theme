//menu
var ww = document.body.clientWidth;
$(document).ready(function() {
  $(".nav li a").each(function() {
    if ($(this).next().length > 0) {
    	$(this).addClass("parent");
		};
	});
	
	$(".toggleMenu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(".nav").toggle();
	});
	adjustMenu();
})
$(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	adjustMenu();
});
var adjustMenu = function() {
	if (ww < 991) {
    // if "more" link not in DOM, add it
    if (!$(".more")[0]) {
    $('<div class="more">&nbsp;</div>').insertBefore($('.parent')); 
    }
		$(".toggleMenu").css("display", "inline-block");
		if (!$(".toggleMenu").hasClass("active")) {
			$(".nav").hide();
		} else {
			$(".nav").show();
		}
		$(".nav li").unbind('mouseenter mouseleave');
		$(".nav li a.parent").unbind('click');
    $(".nav li .more").unbind('click').bind('click', function() {
			
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 991) {
    // remove .more link in desktop view
    $('.more').remove(); 
		$(".toggleMenu").css("display", "none");
		$(".nav").show();
		$(".nav li").removeClass("hover");
		$(".nav li a").unbind('click');
		$(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
};

$('a.gallery2').featherlightGallery({
		galleryFadeIn: 100,
		galleryFadeOut: 300
	});

//Isotope
var $grid = $('.grid').isotope({
  itemSelector: '.element-item',
  layoutMode: 'fitRows',
    percentPosition: true,
});
var filterFns = {};
$('.filters-button-group').on( 'click', 'button', function() {
  var filterValue = $( this ).attr('data-filter');
  // use filterFn if matches value
  filterValue = filterFns[ filterValue ] || filterValue;
  $grid.isotope({ filter: filterValue });
});
$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});


//skrollr
if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && $(window).width() > 767) { // only init skrollr on non-mobile devices
    skrollr.init();
}


//slick
$(document).ready(function(){
    $("h1#diseno").click(function() {
        $("h1#manufactura").removeClass("on");
        $(this).addClass("on");

        $("#tab-manufactura").addClass("off");
        $("#tab-diseno").removeClass("off");
    });
    
    $("h1#manufactura").click(function() {
        $("h1#diseno").removeClass("on");
        $(this).addClass("on");
        $("#tab-diseno").addClass("off");
        $("#tab-manufactura").removeClass("off");
    });
    
    $(".search-ico").click(function(e) {
        e.preventDefault();
        $(this).toggleClass("activo");
        $(".searchbox").toggleClass("ocultar");
    });
    
    $('#slider-home').slick({
    dots: true,
    arrows: false,
    draggable: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true
    });
    $('#slider-productos').slick({
    dots: true,
    arrows: false,
    draggable: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true
    });
    $('.slider-sec').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 680,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
    });
    
    var macy = Macy({
        container: '#grid',
    });
    
});

/*
typer('#consulte')
    .pause(3000)
    .line('Consulte ')
    .pause(300)
    .continue('sin ')
    .pause(200)
    .continue('compromiso')
    .end();
*/

//typer
function neverStop() {
    typer('#textointro')
    .line('imagen, ')
    .pause(400)
    /*.run(function() {
        $('#ico-imagen').addClass('fade-in');}
      )*/
    .continue('forma ')
    .pause(400)
    .continue('y ruido')
    .pause(4000)
    .back('all')
    .run(function(el) {
      el.innerHTML = '';
      document.body.dispatchEvent(new Event('killTyper'));
      setTimeout(() => neverStop(), 10);
    });
}    
neverStop();  




//masonry
var $grid = $('#wrapper-maso, #wrapper-maso-manu').masonry({
  itemSelector: '.item-servicio',
});
$grid.imagesLoaded().progress( function() {
  $grid.masonry('layout');
});

