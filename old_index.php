<?php include 'header.php' ?>
<!--test-->
<div id="intro" class="box fleft fwidth">
    <div class="container-fluid">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12">
            <div class="caja-link box fright text-right">
                <p>
                    <a href="/servicios">Ver</a> 
                    <span class="clearfix"></span>
                    <a href="/servicios">Servicios</a>
                </p>
                <div class="svg">
                    <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="01.--home" transform="translate(-1210.000000, -112.000000)" fill="#363636">
                            <g id="Group-4-Copy-6" transform="translate(1210.000000, 112.000000)">
                                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 titulo">
            <h1>imagen, forma y ruido</h1>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
            <div class="caja-link box fright text-right">
                <p>
                    <a href="/servicios">Ver</a> 
                    <span class="clearfix"></span>
                    <a href="/servicios" >Servicios</a>
                </p>
                <div class="svg">
                    <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="01.--home" transform="translate(-1210.000000, -112.000000)" fill="#363636">
                            <g id="Group-4-Copy-6" transform="translate(1210.000000, 112.000000)">
                                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item-icono">
            
            <?php if( have_rows('primera_columna', 'option') ): while( have_rows('primera_columna', 'option') ): the_row(); ?>	            	
            <div id="ico-imagen" class="box fleft fwidth">
                <div class="img">
                    <img src="<?php the_sub_field('icono_img', 'option'); ?>" alt="">
                </div>
            <div class="clearfix"></div>
            <p><?php the_sub_field('descripcion_txt', 'option'); ?></p> 
            </div>
            <?php endwhile; ?>
	
<?php endif; ?>
            
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item-icono">
            <?php if( have_rows('segunda_columna', 'option') ): while( have_rows('segunda_columna', 'option') ): the_row(); ?>	            	
            <div id="ico-forma" class="box fleft fwidth">
                <div class="img">
                    <img src="<?php the_sub_field('icono_img', 'option'); ?>" alt="">
                </div>
            <div class="clearfix"></div>
            <p><?php the_sub_field('descripcion_txt', 'option'); ?></p> 
            </div>
            <?php endwhile; ?>
	
<?php endif; ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item-icono">
            <?php if( have_rows('tercera_columna', 'option') ): while( have_rows('tercera_columna', 'option') ): the_row(); ?>
            <div id="ico-ruido" class="box fleft fwidth">
                <div class="img">
                    <img src="<?php the_sub_field('icono_img', 'option'); ?>" alt="">
                </div>
            <div class="clearfix"></div>
            <p><?php the_sub_field('descripcion_txt', 'option'); ?></p> 
            </div>
            <?php endwhile; ?>
	
<?php endif; ?>
        </div>
        
    </div>
    
</div>
<div class="clearfix"></div>


<section id="slider-home-wrapper" class="box fleft fwidth">
    <div class="link-visitar-proyectos fwidth">
                <div class="container-fluid">
                    <div class="col-lg-12">
                    <div class="caja-link box fright text-right">
                    <p>
                        <a href="/proyectos">Visitar</a> 
                        <span class="clearfix"></span>
                        <a href="/proyectos">Proyectos</a>
                    </p>
                    <div class="svg">
                    <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="01.--home" transform="translate(-1210.000000, -112.000000)" fill="#FFF">
                            <g id="Group-4-Copy-6" transform="translate(1210.000000, 112.000000)">
                                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                                </g>
                            </g>
                        </g>
                    </g>
                    </svg>
                    </div>
                    
                    </div>
                    </div>
                </div>
            </div>
    <div id="slider-home" class="col-lg-12 no-column slider-container">
        
        
                                        <?php 
  $temp = $wp_query; 
  $wp_query = null; 
  $wp_query = new WP_Query(); 
  $wp_query->query('posts_per_page=10&post_type=proyectos'); 
 
  while ($wp_query->have_posts()) : $wp_query->the_post(); 
?>
        <div class="slide" style="background-image:url(<?php the_field('imagen_principal_para_slider') ?>);">
            <!--index-->
            <!--<div class="wrapper-title fleft fwidth">
                <div class="container">
                    <div class="col-lg-12">
                        <a class="titulo" href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </div>
                </div>
            </div>-->
        </div>
        
        <?php endwhile; ?>
<?php 
$wp_query = null; 
$wp_query = $temp;  // Reset
?>                  

                 
        
    </div>
</section>
        <div class="clearfix"></div>

    <section id="tienda-home" class="container-fluid no-column">
        
    <div class="link-visitar-proyectos fwidth">
                <div class="container-fluid">
                    <div class="col-lg-12">
                    <div class="caja-link box fright text-right">
                    <p>
                        <a href="/tienda">Visitar</a>
                        <span class="clearfix"></span>
                        <a href="/tienda">Tienda</a>
                    </p>
                    <div class="svg">
                    <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="01.--home" transform="translate(-1210.000000, -112.000000)" fill="#363636">
                            <g id="Group-4-Copy-6" transform="translate(1210.000000, 112.000000)">
                                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                                </g>
                            </g>
                        </g>
                    </g>
                    </svg>
                    </div>
                    
                    </div>
                    </div>
                </div>
            </div>
        
        
        
        <?php if( have_rows('apariencia_principal', 'option') ): while( have_rows('apariencia_principal', 'option') ): the_row(); ?>		
        
        <div id="img" class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-column" style="background-image:url(<?php the_sub_field('imagen_principal_tienda_home', 'option'); ?>);">&nbsp;</div>
        <div id="contenido" class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="background-color:<?php the_sub_field('color_de_fondo'); ?>;">
            
            	<?php endwhile; ?>
	
<?php endif; ?>
            
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1 text-center">
                    <?php if ( get_field( 'logo_imagen_tienda_home', 'option' ) ): ?>
 
<img src="<?php the_field('logo_imagen_tienda_home', 'option'); ?>" alt="">
 
<?php else: // field_name returned false ?>
 
<?php if( have_rows('titular_tienda_home', 'option') ): while( have_rows('titular_tienda_home', 'option') ): the_row(); ?>		
            <h1 style="color:<?php the_sub_field('color_del_titular'); ?>;"><?php the_sub_field('titulo_tienda_home'); ?></h1>
<?php endwhile; ?>
<?php endif; ?>                    
                    
 
<?php endif; // end of if field_name logic ?>
                    
                    <?php if( have_rows('bajada_del_producto', 'option') ): while( have_rows('bajada_del_producto', 'option') ): the_row(); ?>		
                    <p style="color:<?php the_sub_field('color_del_texto'); ?>;" ><?php the_sub_field('texto_tienda_home'); ?></p>
                    <?php endwhile; ?>
<?php endif; ?>                    

                    <?php if( have_rows('boton_principal', 'option') ): while( have_rows('boton_principal', 'option') ): the_row(); ?>
                    <a style="border-color:<?php the_sub_field('color_del_boton'); ?>; color:<?php the_sub_field('color_del_boton'); ?>;" href="<?php the_sub_field('link_al_producto'); ?>"><?php the_sub_field('texto_del_boton'); ?></a>
                    <?php endwhile; ?>
<?php endif; ?>     
                </div>
            </div>
        </div>
        
        </section>
        <div class="clearfix"></div>



    <section id="blog-home" class="box fleft fwidth">
        <div class="container-fluid">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-8 titulo">
                <h1>blog</h1>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
            <div class="caja-link box fright text-right">
                <p>
                    <a href="#">Visitar</a>
                    <span class="clearfix"></span>
                    <a href="/blog" >blog</a>
                </p>
                <div class="svg">
                    <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs></defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="01.--home" transform="translate(-1210.000000, -112.000000)" fill="#363636">
                            <g id="Group-4-Copy-6" transform="translate(1210.000000, 112.000000)">
                                <g id="arrow-back" transform="translate(10.000000, 10.000000) scale(-1, 1) translate(-10.000000, -10.000000) ">
                                    <polygon id="Shape" points="20 8.75 4.75 8.75 11.75 1.75 10 0 0 10 10 20 11.75 18.25 4.75 11.25 20 11.25"></polygon>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
                </div>
            </div>
        </div>
            <?php 
  $temp = $wp_query; 
  $wp_query = null; 
  $wp_query = new WP_Query(); 
  $wp_query->query('posts_per_page=8&post_type=post'.'&paged='.$paged); 
 
  while ($wp_query->have_posts()) : $wp_query->the_post(); 
?>
            <div class="box fleft fwidth item-portada-blog">
                <div class="col-lg-4 col-lg-offset-1 col-xs-10 col-xs-offset-1 no-column img" style="background-image:url(<?php the_post_thumbnail_url(); ?>);" >
                    <a href="<?php the_permalink(); ?>">&nbsp;</a>
                </div>
                <div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1 texto">
                    <span class="fecha"><?php the_time('M'); ?> <?php the_time('j'); ?>, <?php the_time('Y'); ?></span>
                    <h2><?php the_title(); ?></h2>
                    <?php the_excerpt(); ?>
                    <p><a href="<?php the_permalink(); ?>">Leer más</a></p>
                </div>
            </div>
            <?php endwhile; ?>
<?php 
$wp_query = null; 
$wp_query = $temp;  // Reset
?>
            
            
        </div>
    </section>
      
<?php include 'footer.php' ?>