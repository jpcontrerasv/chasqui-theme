<?php include 'header.php' ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            


<div id="wrapper" class="single box fleft fwidth">
    
    <div class="container-fluid">
        <div class="col-lg-12 header" style="background-image:url(<?php the_post_thumbnail_url('full'); ?>);">
            &nbsp;
        </div>
        <div class="col-lg-6 col-lg-offset-2 single-galeria">
            <div class="box fleft fwidth txt">
                <span class="fecha"><?php the_time('M'); ?> <?php the_time('j'); ?>, <?php the_time('Y'); ?></span>
                <h1><?php the_title(); ?></h1>
                <div class="clearfix"></div>
                <?php the_content(); ?>
            </div>
        </div>
        <div class="col-lg-3 col-lg-offset-1 relacionadas">
            <h6>Relacionadas</h6>
            <? include 'relacionadas.php' ?>
        </div>
        
        
    </div>
</div>

<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
<?php include 'footer.php' ?>
