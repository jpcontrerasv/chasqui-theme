<!doctype html>
<html lang="en">
  <head>
    <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          
      <?php wp_head(); ?> 
      <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600,700" rel="stylesheet">
  </head>
  <body <?php body_class(); ?> >
      
      <div class="fullwrapper box fleft fwidth">
          
            <?php if ( is_front_page() && is_home() ) { ?>
            <header id="header" class="container-fluid bg-naranjo home">
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-left logo">
          
                  <a href="<?php echo get_site_url(); ?>"><img src="<?php bloginfo('template_url')?>/img/logo-home.svg" alt=""></a>
      
              </div>
                
            <?php } else { ?>
            <header id="header" class="container-fluid pr0">
                
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-left logo">
          
                  <a href="<?php echo get_site_url(); ?>"><img src="<?php bloginfo('template_url')?>/img/logo-interior.svg" alt=""></a>
      
              </div>
                
            <?php } ?>
      
              <div class="col-lg-10 col-md-7 hidden-sm hidden-xs desk-menu">
        
                  <ul class="nav" style="">
            
                        <?php wp_nav_menu( array( 
                        'menu' => 'Menu principal',
                        //'menu_class' => 'nav fleft hidden-lg hidden-md',
                        'container'       => '',
                        'items_wrap' => '%3$s', 
                        'container_class' => false,
                        ) ); ?>
                      
                      <li class="menu-facebook" ><?php if( get_field('facebook_chasqui', 'option') ): ?>
          
                          <a href="<?php the_field('facebook_chasqui', 'option'); ?>" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
                          <?php endif; ?>
                          
                      </li>
          
          
                      <li class="menu-instagram" ><?php if( get_field('instagram_chasqui', 'option') ): ?>
          
                          <a href="<?php the_field('instagram_chasqui', 'option'); ?>" target="_blank"><i class="fa fa-instagram fa-2x"></i></a><?php endif; ?>
                      
                      </li>
                      
                      
                      
                      <?php if (is_cart() || is_checkout_pay_page() || is_checkout() ) { ?>

        
                      <? } else { ?>

        
                      <?php if ( sizeof( $woocommerce->cart->cart_contents ) == 0 ) { ?>

        
                      <?php } else { ?>
          
                      <li>
        
                          <div class="cart-pill hidden-sm hidden-xs" data-start="opacity:1;" data-200-end="opacity:1;" data-50-end="opacity:0;">
            
            
                              <p>
                                  <i class="fa fa-shopping-basket" aria-hidden="true"></i>  Tienes <?php echo sprintf ( _n( '%d producto', '%d productos', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> en tu <a href="<?php echo wc_get_cart_url(); ?>">carrito</a> - <?php echo WC()->cart->get_cart_total(); ?>
                              </p>
            
        
                          </div>
          
                      </li>
          
        
                      <? } ?>
          
        
                      <? } ?>
                      
                  </ul>
      
              </div>
      
              
              
              <div class="hidden-lg hidden-md col-sm-8 col-xs-8 pr0 text-right mov">
          
          
                  <?php /*<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('header-sidebar') ) : endif; ?> */?>

          
                  <p>
              
                      <a href="<?php echo wc_get_cart_url(); ?>">
              
                          <i class="fa fa-shopping-basket" aria-hidden="true"></i> <?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?>
              
                      </a>
                      
                  </p>
          
                  <a class="toggleMenu" href="#" style="display: none; ">Menu</a>
          
                  <ul class="nav" style="">
              
                <?php wp_nav_menu( array( 
                'menu' => 'Menu principal',
                //'menu_class' => 'nav fleft hidden-lg hidden-md',
                'container'       => '',
                'items_wrap' => '%3$s', 
                'container_class' => false,
                ) ); ?>
              
                <li>
                    
                    <?php if( get_field('facebook_chasqui', 'option') ): ?>
                    
                        <a href="<?php the_field('facebook_chasqui', 'option'); ?>" target="_blank"><i class="fa fa-facebook fa-lg"></i></a>
                    
                    <?php endif; ?>
                    
                </li>
              
                <li>
                    
                    <?php if( get_field('instagram_chasqui', 'option') ): ?>
                    
                        <a href="<?php the_field('instagram_chasqui', 'option'); ?>" target="_blank"><i class="fa fa-instagram fa-lg"></i></a>
                    
                    <?php endif; ?>
                    
                </li>
              
                <?php /*
                <li>
                    <?php if ( sizeof( $woocommerce->cart->cart_contents ) == 0 ) { ?>
                    
                        <p>0</p>
                    
                    <?php } else { ?>
                    
                        <p>1</p>
                    
                    <? } ?>
                </li>
                */?>
              
        </ul>

      
              </div>
             
    <!--<div></div>-->
<!--test-->
        
          </header>
       
        
        
        
       
        
              
          
          
          
<div class="clearfix"></div>
      